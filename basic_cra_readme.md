### Overview

In yesterday’s assessment, you extended the project scaffold from [create-react-app](https://github.com/facebook/create-react-app) by creating a new `<Text />` component and using it inside the [render](https://reactjs.org/docs/react-component.html#render) method of the `<App />` component.

For this activity, you’ll be taking the results of that assessment and deploying it to Gitlab Pages. 

In order to deploy to Gitlab Pages, you must create a production build of your create-react-app project. Gitlab contains a CI/CD (continuous integration/continuous deployment) service which you can utilize to automatically create your production build of your create-react-app project whenever you push new commits.

**Step 1**

To activate the Gitlab CI/CD, you must include a `.gitlab-ci.yml` file. You can add one to your top-level project directory with the following contents:
```yml
image: node:alpine

pages:
  stage: deploy
  script:
  - npm install
  - npm run build
  - rm -rf public
  - mv build public
  artifacts:
    paths:
    - public
  only:
  - master
```

*Summary*

If you have not seen `yml` format before just know that it is basically an abbreviated version of JSON that you will find being used for all kinds of configuration files, especially common for CI/CD configurations.

The `image` rule defines what Docker image will be run by Gitlab's server during the CI/CD process. `alpine` refers to the OS that will be on the image, while `node` refers to the primary runtime tool that will be used while this image is running (on a `node` image, we also have `npm` because it comes with the node).

Typical CI/CD configurations are organized by stages, where you can supply terminal commands to run at each stage of the process. For example, the previous example file specifies four terminal commands to run in sequence during the `deploy` stage for the `pages` feature of gitlab.

`artifacts` are files/paths that will be saved after the stage has finished. This is important for this deployment because gitlab pages will look for a `public` folder to serve static assests.

The `only` rule indicates that this `pages` deployment should only be rerun when we have new commits on the `master` branch. This prevents this CI/CD process from running when other git branches are created/updated with new commits.

**Step 2**

Update your `package.json` so that it includes a new top-level property, `"homepage": "https://yourusername.gitlab.io/your-project-name"`

**Step 3**

Write your commit mesage and push your new commit to your gitlab repo. Gitlab's server should recognize that your project now contains a `.gitlab-ci.yml` file, which will cause it to kick off a CI/CD process according to your configuration. You can observe the process progress by going to your gitlab repo in the web browser and finding the CI/CD section (link on the left-hand menu).

Once the CI/CD process has completed successfully, try visiting your Gitlab Pages URL from step 2 in your browser. You'll have completed this activity once you are able to see a live version of your assessment using the Gitlab Pages URL.